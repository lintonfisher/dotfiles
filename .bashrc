# .bashrc

# Get Bash Aliases
if [ -f $HOME/.bash_aliases ]; then
	source $HOME/.bash_aliases
fi

# Set prompt
export PS1='\n[\A] [\w] `nonzero_return`\n$ '

cd $HOME
